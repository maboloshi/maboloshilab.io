# 沙漠之子's Blog

Here are my personal Hexo blog source code.

*Build status of `hexo`*

Github Pages : [![Build Status](https://travis-ci.org/maboloshi/Blog.svg?style=flat-square?branch=hexo)](https://travis-ci.org/maboloshi/Blog)  
GitLab Pages : [![pipeline status](https://gitlab.com/maboloshi/maboloshi.gitlab.io/badges/hexo/pipeline.svg)](https://gitlab.com/maboloshi/maboloshi.gitlab.io/commits/hexo)

- branch `master`: static pages continous delivery by `TravisCI`. [only `Github Pages`]
- branch `hexo`: raw posts and config sources of my hexo blog.